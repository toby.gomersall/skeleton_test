import unittest
from py_multiplier_wrapper.multiplier import py_multiplier

class TestMultiplication(unittest.TestCase):
    def setUp(self):
        print("test:")

    def tearDown(self):
        print("done")

    def test_result(self):
        '''py_multiply should return the product of two integers'''
        self.assertEqual(py_multiplier(3,4), 3*4)

