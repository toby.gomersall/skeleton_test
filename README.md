This README file contains information on the contents of `skeleton_test`.

Please see the corresponding sections below for details.


Dependencies
============

There are no dependencies.

Patches
=======

Please submit any patches against `skeleton_test` to the maintainer:

Maintainer: Toby Gomersall <toby.gomersall@smartacoustics.co.uk>


Table of Contents
=================

  I. About


I. About
========

This is a skeleton SCons environment. It can be used as a basis to setup other projects 

